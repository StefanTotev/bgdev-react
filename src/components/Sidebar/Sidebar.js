import React, { Fragment } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import classes from './Sidebar.scss';
import Backdrop from "../UI/Backdrop/Backdrop";

const sidebar = (props) => {
    let openCloseClass = props.show ? classes.Open : classes.Close;

    return (
        <Fragment>
            <Backdrop show={props.show} clicked={props.closeSidebarHandler}/>
            <div className={[classes.Sidebar, openCloseClass].join(' ')}>
                <div className={classes.Title}>
                    Меню
                </div>
                <ul>
                    <li><a href="/#">Потребители</a></li>
                    <li><a href="/#" data-target="login">Вход</a></li>
                </ul>
                <a href="/#" className={classes.ButtonClose} onClick={props.closeSidebarHandler}>
                    <FontAwesomeIcon icon="arrow-right" size="2x"/>
                </a>
            </div>
        </Fragment>
    );
};

export default sidebar;