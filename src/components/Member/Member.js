import React from 'react';
import classNames from 'classnames/bind';
import styles from './Member.scss';

let cx = classNames.bind(styles);

const member = (props) => {

    const role = props.type;
    const style = cx({
        Member: (role === 'member'),
        Vip: (role === 'vip'),
        Moderator: (role === 'moderator'),
        Admin: (role === 'admin'),
        Bold: props.bold
    });

    return (
        <span className={style}>{props.name}</span>
    );
};

export default member;
