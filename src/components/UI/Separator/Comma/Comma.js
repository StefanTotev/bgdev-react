import React, { Fragment } from 'react';

const comma = () => {
    return (
        <Fragment>
            &#44;
        </Fragment>
    );
};

export default comma;
