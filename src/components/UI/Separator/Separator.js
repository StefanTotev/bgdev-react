import React from 'react';
import Comma from './Comma';

import classes from './Separator.scss';

const separator = (props) => {

    const separators = {
        comma: <Comma />
    };

    return (
        <span className={props.noLast ? classes.NoLastChild : null}>{separators[props.type]}&nbsp;</span>
    );
};

export default separator;
