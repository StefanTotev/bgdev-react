// import React from 'react';
import moment from 'moment';
import 'moment/locale/bg';

const dateTime = (props) => {

    let format = props.format || "DD MMM YYYY, hh:mmч.";

    moment.locale('bg');
    return moment.unix(props.timestamp).format(format);
};

export default dateTime;
