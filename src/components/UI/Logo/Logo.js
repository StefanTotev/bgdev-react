import React from 'react';
import classes from './Logo.scss';
import colors from "../../../assets/styles/colors.scss";

const logo = () => (
    <h1 className={classes.Logo}>
        <a href="/">
            <span className={colors.Orange}>BG</span>
            <span className={colors.Member}>Dev</span>
        </a>
    </h1>
);

export default logo;