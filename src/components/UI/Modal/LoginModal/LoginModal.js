import React, { Fragment } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import classes from './LoginModal.scss';

const loginModal = (props) => {
    return (
        <Fragment>
            <div className={classes.Header}>
                <span className={classes.ButtonClose} onClick={props.modalClosed}><FontAwesomeIcon icon="times" /></span>
                <span className={classes.Title}>Вход</span>
            </div>
            <div className={classes.Body}>
                <form action="/" method="POST">
                    <div className={classes.FormGroup}>
                        <label htmlFor="username">Потребителско име</label>
                        <input type="text" name="username" id="username" className={classes.FormInput}
                               placeholder="Потребителско име"/>
                    </div>
                    <div className={classes.FormGroup}>
                        <label htmlFor="password">Парола</label>
                        <input type="password" name="password" id="password" className={classes.FormInput}
                               placeholder="Парола"/>
                    </div>
                    <div className={classes.RememberMeGroup}>
                        <input type="checkbox" name="rememberme" id="rememberme" className={classes.FormCheckbox}/>
                        <label htmlFor="rememberme">Запомни ме!</label>
                    </div>
                    <div className={classes.RememberMeGroup}>
                        <input type="checkbox" name="privacy" id="privacy" className={classes.FormCheckbox}/>
                        <label htmlFor="privacy">Влез анонимно!</label>
                    </div>
                    <input type="submit" name="submit" className={classes.FormButton} value="Вход"/>
                </form>
            </div>
        </Fragment>
    );
};

export default loginModal;