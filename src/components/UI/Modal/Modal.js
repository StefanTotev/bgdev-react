import React, { Fragment } from 'react';
import Backdrop from '../Backdrop';

import classes from './Modal.scss';
import LoginModal from "./LoginModal/LoginModal";

const modal = (props) => {
    let openCloseClass = props.show ? classes.Open : classes.Close;

    return (
        <Fragment>
            <Backdrop show={props.show} clicked={props.modalClosed}/>
            <div className={[classes.Modal, openCloseClass].join(' ')}>
                <LoginModal modalClosed={props.modalClosed}/>
            </div>
        </Fragment>
    );
};

export default modal;