import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import classes from './Breadcrumb.scss';
import colors from '../../../assets/styles/colors.scss';

const breadcrumb = () => {
    return (
        <section className={classes.Navbar}>
            <div className={classes.Container}>
                <ul className={classes.Breadcrumbs}>
                    <li>
                        <a href="/">Начало</a>
                        {/*<FontAwesomeIcon icon="angle-right" size="md" className={colors.Blue} />*/}
                    </li>
                </ul>
                <div className={classes.MobileBreadcrumbs}>
                    <select>
                        <option value="Начало">
                            Начало
                            <FontAwesomeIcon icon="angle-right" className={colors.Blue} />
                        </option>
                    </select>
                </div>
                <ul className={classes.Submenu}>
                    <li><a href="/">Потребители</a></li>
                    <li><a href="/">Контролен панел</a></li>
                    <li><a href="/">Съобщения</a></li>
                </ul>
            </div>
        </section>
    );
};

export default breadcrumb;
