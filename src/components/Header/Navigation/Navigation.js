import React from 'react';
import classes from "./Navigation.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const navigation = (props) => {
    return (
        <nav>
            <a href="/#" className={classes.MobileNav} onClick={props.mobileNavClickHandler}>
                <FontAwesomeIcon icon="bars"/>
            </a>
            <ul className={classes.TopNavigation}>
                {/* if logged in */}
                {/*<li><a href="/member/NAME" className={classes.TopNavLink}>NAME</a></li>*/}
                {/*<li><a href="/logout" className={classes.TopNavLink}>Изход</a></li>*/}
                {/* else */}
                <li><a href="/#" className={classes.TopNavLink} data-target="login" onClick={props.topNavClickHandler}>Вход</a></li>
                <li><a href="/#" className={classes.TopNavLink}>Регистрация</a></li>
            </ul>
        </nav>
    );
};

export default navigation;