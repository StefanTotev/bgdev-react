import React from 'react';
import Navigation from './Navigation';
import Logo from '../UI/Logo';

import classes from './Header.scss';

const header = (props) => {
    return (
        <header className={classes.Header}>
            <div className={[classes.Container, classes.Flex].join(' ')}>
                <Logo/>
                <Navigation topNavClickHandler={props.topNavClickHandler} mobileNavClickHandler={props.mobileNavClickHandler}/>
            </div>
        </header>
    );
};

export default header;
