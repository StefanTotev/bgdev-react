import React, { Component, Fragment } from 'react';
import Member from '../Member';
import Separator from '../UI/Separator';
import DateTime from '../UI/DateTime';
import Status from '../../models/Status';
import classNames from 'classnames/bind';
import styles from './Footer.scss';

let cx = classNames.bind(styles);

class Footer extends Component {
    state = {
        status: null,
        online: null
    };

    componentDidMount() {
        console.log('[Footer] componentDidMount()');

        const requestStatus = new Status();

        requestStatus.then(res => this.setState({
                status: res.status,
                online: res.online
            }))
            .catch(err => console.log(err));

    }

    render() {
        console.log('[Footer] render()');

        if ( !this.state.status || !this.state.online ) {
            return null;
        }

        let members = null;

        if ( this.state.online.members.length > 0 ) {
            members = this.state.online.members.map(member => {
                return (
                    <Fragment>
                        <Member key={member} name={member} type="member" href="/#" />
                        <Separator type="comma" noLast />
                    </Fragment>
                );
            });
        } else {
            members = 'Няма';
        }

        const containerStyles = cx({
            Container: true,
            Flex: true
        });

        return (
            <footer className={styles.Footer}>
                <div className={containerStyles}>
                    <div className={styles.Box}>
                        <h3>Статистика</h3>
                        <div>
                            Потребителите ни са публикували общо <b>{(this.state.status.total_replies + this.state.status.total_topics)}</b> мнения
                        </div>
                        <div>Имаме <b>{this.state.status.mem_count}</b> регистрирани потребители</div>
                        <div>
                            Най-новият потребител е <Member name={this.state.status.last_member_name} type="member" href="/#" bold />
                        </div>
                        <div>
                            Най-голямата посещаемост на форума (<b>{this.state.status.most_count}</b> потребители) беше на
                            <b> <DateTime timestamp={this.state.status.most_date} /></b>
                        </div>
                    </div>
                    <div className={styles.Box}>
                        <h3>Кой е онлайн?</h3>
                        <div>
                            Общо потребители на линия:&nbsp;
                            {this.state.online.guests} Гости,&nbsp;
                            {this.state.online.members.length} Регистрирани,&nbsp;
                            {this.state.online.anonymous} Анонимни
                        </div>
                        <div>
                            Регистрирани потребители:&nbsp;
                            {members}
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
