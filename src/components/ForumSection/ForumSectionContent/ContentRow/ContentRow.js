import React from 'react';
import classes from "./ContentRow.scss";
import Icon from "./Icon/Icon";
import Title from "./Title/Title";
import LastPoster from "./LastPoster/LastPoster";

const contentRow = (props) => {
    return (
        <div className={classes.Row}>
            <Icon isRead={true} isOpen={parseInt(props.data.status)} />
            <Title
                name={props.data.name}
                description={props.data.description}
                recentTopicTitle={props.data.last_title}
                author={props.data.last_poster_name}
                recentReplyDate={props.data.last_post}
                topics={props.data.topics}
                posts={props.data.posts}
            />
            <div className={classes.Stats}>
                <div>{props.data.topics} теми</div>
                <div>{props.data.posts} мнения</div>
            </div>
            <LastPoster/>
        </div>
    );
};

export default contentRow;
