import React from 'react';
import Member from '../../../../Member';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classes from "./LastPoster.scss";

const lastPoster = () => {
    return (
        <div className={classes.LastPoster}>
            <div>
                <a href="/#" className={classes.ForumSectionsLink} title="Към последното мнение">
                    <FontAwesomeIcon icon="angle-right"/> Някакво заглавие
                </a>
            </div>
            <div>
                <a href="/#" className={classes.ForumSectionsLink} title="Виж профила">
                    <FontAwesomeIcon icon="user" size="xs"/>&nbsp;
                    <Member name="relax4o" type="admin" />
                </a>
            </div>
            <div>
                <FontAwesomeIcon icon="clock" size="sm"/> 26 Mar 2018, 19:18
            </div>
        </div>
    );
};

export default lastPoster;
