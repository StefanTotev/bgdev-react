import React from 'react';
import classes from "./Title.scss";
import DateTime from "../../../../UI/DateTime/DateTime";

const title = (props) => {
    return (
        <div className={classes.Title}>
            <span className={classes.SubForumName}>
                <a href="/#" className={classes.ForumSectionsLink}>{props.name}</a>
            </span>
            <span className={classes.StatsMobile}>{props.topics} Теми {props.replies} Мнения</span>
            <div className={classes.Description}>{props.description}</div>
            <div className={classes.LastPosterMobile}>
                <span>
                    <a href="/#" className={classes.ForumSectionsLink}
                       title="Към последното мнение">{props.recentTopicTitle}</a>
                </span>
                <span>&nbsp;от&nbsp;
                    <a href="/#" className={classes.ForumSectionsLink} title="Виж профила">{props.author}</a>,
                </span>
                <span>&nbsp;<b><DateTime timestamp={props.recentReplyDate} /></b></span>
            </div>
        </div>
    );
};

export default title;
