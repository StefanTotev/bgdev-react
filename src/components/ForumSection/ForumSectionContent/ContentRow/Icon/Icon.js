import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from './Icon.scss';

const icon = (props) => {

    let style = (props.isRead && props.isOpen) ? 'far' : 'fas';
    let icon = props.isOpen ? 'comment' : 'lock';
    let colour = props.isOpen ? '#ff7000' : '#bc0000';

    return (
        <div className={styles.Icon}>
            <FontAwesomeIcon icon={[style, icon]} color={colour} size="2x"/>
            {/*<i className={ true ? 'far fa-comment orange' : 'fas fa-lock red'} fa-2x"></i>*/}
        </div>
    );
};

export default icon;
