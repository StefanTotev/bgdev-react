import React from 'react';
import classNames from 'classnames/bind';
import styles from "./ForumSectionContent.scss";

let cx = classNames.bind(styles);

const forumSectionContent = (props) => {

    const style = cx({
        Content: true,
        Hide: !props.isOpen
    });

    return (
        <div className={style}>
            {props.children}
        </div>
    );
};

export default forumSectionContent;
