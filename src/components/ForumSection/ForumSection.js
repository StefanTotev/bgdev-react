import React, { Component } from 'react';
import classes from "./ForumSection.scss";

import ForumSectionHeader from './ForumSectionHeader';
import ForumSectionContent from './ForumSectionContent';
import ContentRow from './ForumSectionContent/ContentRow';
import Category from "../../models/Category";

class ForumSection extends Component
{
    state = {
        category: null,
        forums: null,
        online: null,
        isSectionOpened: true
    };

    componentDidMount() {
        console.log('[ForumSection] componentDidMount()');

        const requestCategory = new Category(this.props.id);

        requestCategory.then(res => this.setState({
            category: res.category,
            forums: res.forums,
            online: res.online
        }));
    }

    onToggle = () => {
        this.setState({
            isSectionOpened: !this.state.isSectionOpened
        });

        console.log(this.state.isSectionOpened);
    };

    render() {

        if ( !this.state.category ) {
            return null;
        }

        const subForums = this.state.forums.map( subForum => <ContentRow key={subForum.id} data={{...subForum}} />);

        return (
            <section className={classes.Section}>
                <div className={classes.Container}>
                    <ForumSectionHeader title={this.state.category.name} onToggle={this.onToggle} isOpen={this.state.isSectionOpened}/>
                    <ForumSectionContent isOpen={this.state.isSectionOpened}>
                        {subForums}
                    </ForumSectionContent>
                </div>
            </section>
        );
    }
}

export default ForumSection;
