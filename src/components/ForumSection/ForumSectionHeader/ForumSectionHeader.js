import React from 'react';
import classes from "./ForumSectionHeader.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const forumSectionHeader = (props) => {

    let icon = props.isOpen ? 'angle-down' : 'angle-left';

    return (
        <div className={classes.Header}>
            <a href="/#">{props.title}</a>
            <div className={classes.Controls}>
                <span className={classes.ButtonToggle} onClick={props.onToggle}>
                    <FontAwesomeIcon icon={icon} size="lg"/>
                </span>
            </div>
        </div>
    );
};

export default forumSectionHeader;
