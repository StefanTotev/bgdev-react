import React, { Component, Fragment } from "react";
import Header from "../../components/Header/Header";
import Breadcrumb from "../../components/Header/Breadcrumb/Breadcrumb";
import Footer from "../../components/Footer/Footer";
import Modal from "../../components/UI/Modal/Modal";
import Sidebar from "../../components/Sidebar/Sidebar";
import Members from "../../models/Members";

import classes from "./Layout.scss";

class Layout extends Component {
    state = {
        modal: {
            show: false,
            type: null
        },
        isSidebarShown: false
    };

    componentDidMount() {
        console.log("[Layout] componentDidMount()");

        const requestMembers = new Members();

        requestMembers.then(res => {
            console.log(requestMembers.findMemberById(res, 12));
        });
    }

    modalClosedHandler = () => {
        this.setState({
            modal: {
                show: false,
                type: null
            }
        });
    };

    topNavClickHandler = ( event ) => {
        event.preventDefault();

        if ( event.currentTarget.dataset.target ) {
            this.setState({
                modal: {
                    show: true,
                    type: event.currentTarget.dataset.target
                }
            });
        }
    };

    toggleMobileNavClickHandler = () => {
        this.setState(( prevState ) => {
            return {
                isSidebarShown: !prevState.isSidebarShown
            };
        });
    };

    render() {
        console.log("[Layout] render()");

        return (
            <Fragment>
                <Header topNavClickHandler={this.topNavClickHandler}
                        mobileNavClickHandler={this.toggleMobileNavClickHandler} />
                <Breadcrumb />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
                <Footer />
                <Modal show={this.state.modal.show}
                       modalClosed={this.modalClosedHandler}
                       type={this.state.modal.type} />
                <Sidebar show={this.state.isSidebarShown} closeSidebarHandler={this.toggleMobileNavClickHandler} />
            </Fragment>
        );
    }
}

export default Layout;
