import React, { Component, Fragment } from 'react';
import ForumSection from '../../components/ForumSection';
import Categories from "../../models/Categories";

class Forum extends Component
{
    state = {
        result: null
    };

    componentDidMount() {
        console.log("[Forum] componentDidMount()");

        const requestCategories = new Categories();

        requestCategories.then(res => this.setState({
            result: res
        }));
    }

    render() {
        console.log("[Forum] render()");

        if ( !this.state.result ) {
            return null;
        }

        const categories = this.state.result.map( category =>
                <ForumSection
                    key={category.id}
                    id={category.id}
                    isOpen={true}
                />
            );

        return (
            <Fragment>
                {categories}
            </Fragment>
        );
    }
}

export default Forum;
