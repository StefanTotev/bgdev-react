import AbstractRequest from "./AbstractRequest";

class Category extends AbstractRequest
{
    static ENDPOINT = '/category/';

    constructor(id) {
        super(Category.ENDPOINT + id);
    }

    onSuccess(response) {
        return response.data;
    }

    onError(err) {
        return err;
    }
}

export default Category;
