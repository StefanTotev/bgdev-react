import AbstractRequest from "./AbstractRequest";

class Categories extends AbstractRequest
{
    static ENDPOINT = '/categories';

    constructor() {
        super(Categories.ENDPOINT);
    }

    onSuccess(response) {
        return response.data;
    }

    onError(err) {
        return err;
    }
}

export default Categories;
