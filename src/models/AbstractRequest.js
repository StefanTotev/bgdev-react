import api from '../axios/api';

class AbstractRequest
{
    constructor(endPoint) {
        const objectName = this.constructor.name

        if ( !(this instanceof AbstractRequest) ) {
            throw new TypeError( objectName + ' is not an instance of AbstractRequest.');
        }

        if ( this.onSuccess === undefined ) {
            throw new TypeError(objectName + ".onSuccess() is not implemented.");
        }

        if ( this.onError === undefined ) {
            throw new TypeError(objectName + ".onError() is not implemented.");
        }

        return new Promise((resolve, reject) => {
            api.get(endPoint)
                .then(res => resolve(this.onSuccess(res)))
                .catch(err => reject(this.onError(err)));
        });
    }
}

export default AbstractRequest;
