import AbstractRequest from "./AbstractRequest";

class Members extends AbstractRequest {
    static ENDPOINT = "/members";

    constructor() {
        super(Members.ENDPOINT);
    }

    onSuccess( response ) {
        return response.data;
    }

    onError( err ) {
        return err;
    }

    findMemberById = ( members, id ) => (
        members.find(el => el.id === id)
    );

    findMemberByName = ( members, name ) => (
        members.find(el => el.name === name)
    );
}

export default Members;
