import AbstractRequest from './AbstractRequest';

class Status extends AbstractRequest
{
    static ENDPOINT = '/status';

    constructor() {
        super(Status.ENDPOINT);
    }

    onSuccess(response) {
        return {
            status: response.data.status,
            online: this.getOnlineMembers(response)
        };
    }

    onError(err) {
        return err;
    }

    getOnlineMembers(response) {
        let usersData = {
            anonymous: 0,
            guests: 0,
            members: []
        };

        usersData = response.data.online.map(el => {
            if ( el.member_id === 0 ) {
                if ( el.member_name === 'Annon' ) {
                    usersData.anonymous += el.members_count;
                } else {
                    usersData.guests += el.members_count;
                }
            } else {
                usersData.members.push(el.member_name);
            }

            return usersData;
        });

        return usersData[0];
    }
}

export default Status;
