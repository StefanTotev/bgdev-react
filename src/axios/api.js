import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://bgdev.saas.bg/forums/api',
    withCredentials: true
});

export default instance;