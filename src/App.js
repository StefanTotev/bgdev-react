import React, { Component } from 'react';
import Layout from './containers/Layout';
import Forum from './containers/Forum';

class App extends Component {
    render() {
        return (
            <Layout>
                <Forum />
            </Layout>
        );
    }
}

export default App;
