import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faSearch,
    faBars,
    faTimes,
    faAngleRight,
    faAngleLeft,
    faAngleDown,
    faArrowRight,
    faComment,
    faLock,
    faUser,
    faClock,
} from '@fortawesome/free-solid-svg-icons';
import { faComment as farComment } from '@fortawesome/free-regular-svg-icons';

import * as serviceWorker from './serviceWorker';

library.add(faSearch, faBars, faTimes, faAngleRight, faAngleLeft, faAngleDown, faArrowRight, faComment, faLock, faUser, faClock, farComment);

ReactDOM.render(<App/>, document.getElementById('root'));

serviceWorker.unregister();
